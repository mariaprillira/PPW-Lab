# Lab 8 : Introduction _Introduction to Oauth2_

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *

## Learning Goals

After doing this lab, students are expected to understand:
- OAuth2 concept
- OAuth2 usage

## Lab Final Result
- Use facebook login to call Facebook API.
- Use facebook login to display and store user data.
- Create a feature that can post to a facebook timeline through Facebook API.

## Introduction to Oauth2

_OAuth 2.0 is the industry-standard protocol for authorization_

#### What is Oauth2?
OAuth 2.0 is the industry-standard protocol for authorization.  Oauth2 is an authorization framework that can provide user data from a certain platform to an application which can be used to call an API that provided by that platform. Some platforms that implement Ouath are Facebook, Twitter, Google, etc.  

Generally, Ouath2 has some roles, those are:
Resource Owner : Resource owner is a platform user which gives an access to some application.

Resource/Authorization server :  Resource server is a platform provider, resource server provide Oauth2 service and API.

Client/Application : Client is an application, whether a mobile or web application, that wants to use API from resource/authorization server.


#### How does Oauth2 Work?
Generally, these are Oauth2 implementation steps:

![abstract flow](https://assets.digitalocean.com/articles/oauth/abstract_flow.png "abstract flow")

1. Application do an authorization request to the user to get a permission which will be used to access the API service.
2. If the user allowed the request, then the application will get an authorization grant.
3. The application sends a request to get an access token from the authorization server using the authorization grant and the application identitiy
4. If the application identity and the authorization grant are valid, then the authorization server will give an access token to the application, in this step the authorization is done.
5. The application calls the API from the resource server, using the access token that has been given before.
6. If the access token is valid, then the server will give a response that corresponds to that API call.


#### Facebook Login
In this lab tutorial, we are going to learn to use an API from a certain social media, which is facebook. Facebook has implemented their own oauth framework which is called Facebook login. We are going to use javascript sdk that has been provided by facebook to make us easier to authorize and to call Facebook API.

To use the API from facebook, first we have to create and register our application at facebook. These are steps to register and setting the application:

1. Open https://developers.facebook.com/
2. Register as a Facebook Developer
3. Create a new application by clicking **Add a New App** button which can be found at top right corner.
4. Go to the application page that has been created and select **Add Product**, then add **Facebook Login**.
5. At setting section, add a platform and select website, then fill in site url.
   Another way: You can find **Quickstart** page, select **Web** as a platform.
6. The application has been successfully registered.

After registering the application, we are going to try some features from Facebook API using oauth (Facebook Login)

*Create login with facebook feature*

1. Create `lab8.js` file.
Firstly create a file `lab8.js`, then fill in `lab8.js` with this code snippet.

```javascript
 window.fbAsyncInit = function() {
    FB.init({
      appId      : '{your-app-id}',
      cookie     : true,
      xfbml      : true,
      version    : '{latest-api-version}'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

```

The code snippet above is used to initialize and load a javascript sdk asynchronously.

_The Facebook SDK for JavaScript doesn't have any standalone files that need to be downloaded or installed, instead you simply need to include a short piece of regular JavaScript in your HTML that will asynchronously load the SDK into your pages_

_Do not forget to add `lab8.js` to your base.html_

2. Change _{your-app-id}_ and _{latest-api-version}_ with **App ID** and **Api Version** that can be obtained from your application **Dashboard** at https://developers.facebook.com/

3. Create `lab_8.html` file, then add Login with Facebook button.

```html
<button onclick="facebookLogin()">Login with Facebook</button>
```

And then add this function to `lab8.js` file which has been created before.

```javascript
function facebookLogin(){
     FB.login(function(response){
       console.log(response);
     }, {scope:'public_profile'})
   }
```

That ```facebookLogin()``` function will call login function from FB instance (instance from facebook SDK) which is used to authenticate and to authorize a user, this FB.login() function also has a scope parameter which can setting what kind of permission that our application want, as an example above we have added ```public_profile``` permission so that our aplikasi can access our id, name, first_name, last_name etc (see at [here](https://developers.facebook.com/docs/facebook-login/permissions#reference-public_profile)). We can view existing permissions with this link
https://developers.facebook.com/docs/facebook-login/permissions

This is a response from the above function:

```json
{
    status: 'connected',
    authResponse: {
        accessToken: '...',
        expiresIn:'...',
        signedRequest:'...',
        userID:'...'
    }
}
```

For more detail, we can view the documentation about facebook javascript sdk's function and method, with this link https://developers.facebook.com/docs/javascript/reference/v2.11

We can also view existing permissions list with this link
https://developers.facebook.com/docs/facebook-login/permissions

_Display user information_

In order to display user information, we can use Facebook Graph API, the following is an example of ```getUserData()``` function to get user information.

```javascript
function getUserData(){
   FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.api('/me?fields=id,name', 'GET', function(response){
            console.log(response);
          });
        }
    });
}
```

The api() method from FB instance will do a GET request to url path ‘me?fields=id,name�? which will return this response.

```json
{
  id: "1680341582010118",
  name: "Igun Nugi"
}
```

You can add fields that need to be returned on demand, but adjusted with permissions that has been set when login to facebook (see again facebookLogin() function). More about graph API can be seen at this link https://developers.facebook.com/docs/graph-api/reference/

_Create a Post to the timeline_

1. Change the permissions in facebookLogin function at `lab8.js` like the following code:

```javascript
function facebookLogin(){
     FB.login(function(response){
       console.log(response);
     }, {scope:'public_profile,user_posts,publish_actions'})
   }
```

By adding `user_posts` and `publish_actions` permissions, what kind of actions that our application can do by using Graph API?

To post a status to the facebook timeline, we can use Graph API which is used by facebook, this is the example

```javascript
function postFeed(){
     var message = "Hello World!";
     FB.api('/me/feed', 'POST', {message:message});
 }
```

2. Login again and try to post something at facebook

_Create logout feature_

To create logout feature we can use logout method from FB instance, this is the example to implement logout function

```javascript
function facebookLogout(){
     FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout();
        }
     });
 }

```

## lab8.js template

You can use lab8.js template below to start doing the lab8

```javascript
  // FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '{your app id}',
      cookie     : true,
      xfbml      : true,
      version    : '{your api version}'
    });

    // implement a function that check login status (getLoginStatus)
    // and run render function below, with a boolean true as a paramater if
    // login status has been connected

    // This is done because when the user opened the web and the user has been logged in,
    // it will automatically display the logged in view
  };

  // Facebook call init. default from facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Render function, receive a loginFlag paramater that decide whether
  // render or create a html view for the logged in user or not
  // Modify this method as needed if you feel you need to change the style using
  // Bootstrap's classes or your own implemented CSS
  const render = loginFlag => {
    if (loginFlag) {
      // If the logged in view the one that will be rendered

      // Call getUserData method (see below) that have been implemented by you with a callback function
      // that receive user object as a parameter.
      // This user object is the response from an API facebook call.
      getUserData(user => {
        // Render profile view, post input form, post status button, and logout button
        $('#lab8').html(
          '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
          '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
          '<button class="logout" onclick="facebookLogout()">Logout</button>'
        );

        // After renderin the above view, get home feed data from the logged in account
        // by calling getUserFeed method which you implement yourself.
        // That method has to receive a callback parameter, which receive a feed object as a response
        // from calling the Facebook API
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render the feed, customize as needed.
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                  '<h2>' + value.story + '</h2>' +
                '</div>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h2>' + value.story + '</h2>' +
                '</div>'
              );
            }
          });
        });
      });
    } else {
      // The view when not logged in yet
      $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
    }
  };

  const facebookLogin = () => {
    // TODO: Implement this method
    // Make sure this method receive a callback function that will call the render function
    // that will render logged in view after successfully logged in,
    // and this function has all needed permissions at the above scope.
    // You can modify facebookLogin function above
  };

  const facebookLogout = () => {
    // TODO: Implement this method
    // Make sure this method receive a callback function that will call the render function
    // that will render not logged in view after successfully logged out.
    // You can modify facebookLogout function above
  };

  // TODO: Complete this method
  // This method modify the above getUserData method that receive a callback function called fun
  // and the request user data from the logged in account with all the needed fields in render method,
  // and call that callback function after doing the request and forward the response to that callback function.
  // What does it mean by a callback function?
  const getUserData = (fun) => {
    ...
    FB.api('/me?fields=....', 'GET', function (response){
      fun(response);
    });
    ...
  };

  const getUserFeed = (fun) => {
    // TODO: Implement this method
    // Make sure this method receive a callback function parameter and do a data request to Home Feed from
    // the logged in account with all the needed fields in render method, and call that callback function
    // after doing the request and forward the response to that callback function.
  };

  const postFeed = () => {
    // Todo: Implement this method,
    // Make sure this method receive a string message parameter and do a POST request to Feed
    // by going through Facebook API with a string message parameter as a message.
  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
  };
```

About this template, if you saw `() => {}` syntax, that is a form of arrow function that has been introduced by
EcmaScript 6 or ES6 or ES2015 JavaScript version. Actually arrow function is just another form of `function() {}` syntax in JavaScript.

Syntax Arrow Function `(parameter1, parameter2) => { code }` sebenarnya hanyalah bentuk lain dari `function(parameter1, parameter 2) { code }`.

The syntax `const` is another form of `var`, but `const` marks that the data in that varibale can not be changed, and there is `let`, inverse from `const` where the data can be changed afterward. These two forms are best practices that are being used for EcmaScript 6 or ES6 or ES2015 JavaScript version.

The syntax `const funcName = parameter1 => { code }` is just another form of `function funcName(parameter1) { code }`.

For more information about EcmaScript 6, you can read this article http://es6-features.org

## Create OauthFacebook Page
1. Do not forget to run your virtual environment
2. Create new _apps_ called `lab_8`
3. Add `lab_8` to `INSTALLED_APPS`
4. Create a new _Test_ into `lab_8/tests.py` :
5. _Commit_ and then _Push_ your work, then you will see that your _UnitTest_ will be _error_
6. Create URL configuration at `praktikum/urls.py` for `lab_8`
    ```python
        ........
        import lab_8.urls as lab_8

        urlpatterns = [
            .....
            url(r'^lab-8/', include(lab_8, namespace='lab-8')),
        ]
    ```

7. Create URL configuration at `lab_8/urls.py`
    ```python
    from django.conf.urls import url
    from .views import index

    urlpatterns = [
        url(r'^$', index, name='index'),
    ]
    ```

8. Register the application to the Facebook Developer Page.
9. Implement a login feature like in the above tutorial by completing `lab_8.html` and `lab8.js`
10. Implement a facebook post feature at `lab_8.html` page and `lab8.js`
11. Implement a logout feature at `lab_8.html` and `lab8.js`
12. Deploy to heroku and do not forget to change the application hostname at seeting page on facebook developers admin
page with your herokuapp url.
13. Do not forget to be happy! <3

*DISCLAIMER*
Make sure you are using newest version browser of Google Chrome / Mozilla Firefox (Chrome 45++ or Firefox 22++) because JavaScript that used in this lab is a modern JavaScript and not all browsers can run this.

## Checklist

### Mandatory
1. Create a page for Login using OAuth
    1. [ ] Register the application to facebook developer page
    2. [ ] Do an OAuth Login using Facebook
    3. [ ] Display user information from the logged in user using API
    4. [ ] Do a facebook status post
    5. [ ] Display post status at lab_8.html
    6. [ ] Do a Logout
    7. [ ] Implement a beautiful and responsive css
    
2. Answer the questions from this document by writing it at your note book or source code that will be presented at demo

3. Make sure you have a good _Code Coverage_
    1. [ ] If you are not yet configure the _Code Coverage_ at Gitlab, then see the steps `Show Code Coverage in Gitlab` at [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md)
    2. [ ] Make sure your _Code Coverage_ is 100%


### Additional

1. Do a delete status at facebook page
    1. [ ] Implement delete button at post status list
    2. [ ] Do a delete post status using existing Facebook API
